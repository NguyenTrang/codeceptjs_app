const { I, LoginPage } = inject();
const LoginPageDemo = require('../pages/login')
module.exports = {

  // insert your locators and methods here
  
  login(username, passWord)
  {
    I.amOnPage('');
    I.waitForText('GitHub', 10);
    I.seeElement(LoginPage.login.userName);
    I.fillField(LoginPage.login.userName, username);
    I.seeElement(LoginPage.login.passWord);
    I.fillField(LoginPage.login.passWord, passWord);
  }
}
