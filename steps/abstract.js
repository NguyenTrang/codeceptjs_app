const { I, } = inject();
const TimePage = require('../pages/time.js',
	module.exports = {

		tapButton(element) {
			I.waitForVisible(element, TimePage.time.button);
			I.tap(element);
		},

		fillFieldInput(element, value) {
			I.waitForElement(element, TimePage.time.input);
			I.fillField(element, value);
		},

		seeText(text) {
			I.waitForText(text, TimePage.time.text);
			I.see(text);
		},

		async isElementDisplayed(xpath){
			I.waitForElement(xpath, TimePage.time.button);
			const value = await I.grabNumberOfVisibleElements(xpath);
			return value;
		},

	},
);