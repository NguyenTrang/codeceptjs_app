const { I } = inject();
const sendMessagePage = require('../pages/sendMessage.js');
const abstract = require('../steps/abstract.js');
module.exports = {
	addGroupButton() {
		abstract.tapButton(sendMessagePage.sendMessage.contacts);
	},
	searchMemberOnTeam(nameMember) {
		abstract.tapButton(sendMessagePage.sendMessage.team);
		abstract.fillFieldInput(sendMessagePage.sendMessage.search, nameMember);
		abstract.tapButton(sendMessagePage.sendMessage.contact_0);
	},
	tapOnChatMessage() {
		abstract.seeText(sendMessagePage.sendMessage.textContactProfile);
		abstract.tapButton(sendMessagePage.sendMessage.chat);
	},
	sendMessageChat(message) {
		abstract.fillFieldInput(sendMessagePage.sendMessage.input, message);
		abstract.tapButton(sendMessagePage.sendMessage.btnSend);
		abstract.seeText(message);
	},
	sendVoidMessage() {
		abstract.tapButton(sendMessagePage.sendMessage.btnRecord);
		abstract.tapButton(sendMessagePage.sendMessage.allow);
		I.swipeUp(sendMessagePage.sendMessage.btnRecord);
		abstract.tapButton(sendMessagePage.sendMessage.btnSend);
	},

	sendCallMessage()
	{
		abstract.tapButton(sendMessagePage.sendMessage.btnCall);
		abstract.tapButton(sendMessagePage.sendMessage.endCall);
	},
};
