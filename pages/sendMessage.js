const { I } = inject();

module.exports = {
    sendMessage:
        {
            contacts: '//android.view.ViewGroup[@content-desc="bottomTab_contact"]/android.widget.TextView',
            team: '//android.view.ViewGroup[@content-desc="contact_tab_teams"]/android.widget.TextView',
            search: '//android.widget.EditText[@content-desc="contact_search"]',
            contact_0: '//android.view.ViewGroup[@content-desc="teams_0_contact_0"]',
            textContactProfile: "Contact Profile",
            chat: '//android.view.ViewGroup' + '[@content-desc="profile_chat"]',
            input: '//android.widget.EditText[@content-desc="chatDetail_input"]',
            btnSend: '//android.view.ViewGroup[@content-desc="chatDetail_sendMessage"]',
            btnRecord: '//android.view.ViewGroup[@content-desc="chatDetail_record"]/android.widget.TextView',
             btnCall: '//android.view.ViewGroup[@content-desc="chatDetail_call"]',
            allow: '//android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button[2]',
            endCall: '//android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[6]/android.widget.TextView',
        }
};

