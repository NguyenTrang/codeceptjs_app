const { I } = inject();
const LoginPageDemo = require('../pages/login_Mobile.js');
const abstract = require('../steps/abstract.js');
const { xpathCode } = require('../pages/login_Mobile.js');

module.exports = {

	tapSkipButton() {
		abstract.tapButton(LoginPageDemo.login.skipBtn);
	},

	fillFieldCode(code) {
		let lengthCode = code.length;

		for (let i = 0; i < lengthCode; i++) {
			abstract.fillFieldInput(LoginPageDemo.xpathCode(i), code.charAt(i));
		}
	},

	fillFieldOtp(otp) {
		let lengthOtp = otp.length;
		for (let j = 0; j < lengthOtp; j++) {
			abstract.fillFieldInput(LoginPageDemo.xpathOtp(j), otp.charAt(j));
		}
	},

	fillFieldPassword(passWord) {
		abstract.fillFieldInput(LoginPageDemo.login.password, passWord);
		abstract.tapButton(LoginPageDemo.login.signIn);
	},
	seeTextDashboard() {
		abstract.seeText(LoginPageDemo.login.textDashboard);
	},

	updateAppLater() {
		abstract.seeText(LoginPageDemo.login.textWelcome);
		let value = this.updateAppLater2();
		if (value) {
			abstract.tapButton(LoginPageDemo.login.later);
		}
		else {
			console.log('sai roi ' + value);
		}
	},

	async updateAppLater2() {
		let value = await I.grabNumberOfVisibleElements(LoginPageDemo.login.later);
		return value;
	}
};
