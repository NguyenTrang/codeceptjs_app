const { I } = inject();

module.exports = {
  login: {
    url: "",
    userName : "//input[@id='user[login]']",
    email :  "//input[@id='user[email]' and @name = 'user[email]']",
    passWord : "(//input[@name = 'user[password]'])[1]",
    signUpButton: "(//button[text()='Sign up for GitHub'])[1]"
  }
 
  // insert your locators and methods here
}
