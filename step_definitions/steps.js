const { I } = inject();
const LoginMobileStep = require('../steps/login_Mobile.js');
const sendMessageStep = require('../steps/sendMessage.js');

let code  = '0jsx3i43u9';
let passWord = 'Testing@123';
let otp = '111111';
let nameMember = 'automation_auto_30';
let messageChat = "Qa test send message";

Given('I login on leap work', () => {
    LoginMobileStep.tapSkipButton();
    LoginMobileStep.fillFieldCode(code);
    LoginMobileStep.fillFieldPassword(passWord);
    LoginMobileStep.fillFieldOtp(otp);
    LoginMobileStep.seeTextDashboard();
});
Then('I send messenger to Account Manage', () => {
    sendMessageStep.addGroupButton();
    sendMessageStep.searchMemberOnTeam(nameMember);
    sendMessageStep.tapOnChatMessage();
    sendMessageStep.sendMessageChat(messageChat);
});

Then('I send record to Account Manage', () => {
    sendMessageStep.addGroupButton();
    sendMessageStep.searchMemberOnTeam(nameMember);
    sendMessageStep.tapOnChatMessage();
    sendMessageStep.sendVoidMessage();
});
Then('I call on message to Account Manage', () => {
    sendMessageStep.addGroupButton();
    sendMessageStep.searchMemberOnTeam(nameMember);
    sendMessageStep.tapOnChatMessage();
    sendMessageStep.sendCallMessage();
});