const event = require('codeceptjs').event
require('./local_data')
exports.config = {
  rerun: {
    // run 6 times until 1st success
    minSuccess: 1,
    maxReruns: 6,
  },
  output: './output',
  helpers: {
    Appium: {
      platform: 'Android',
      device: 'emulator-5554',
      automationName: 'Appium',
      desiredCapabilities: {
        appPackage: 'com.leapxpert.manager.qa',
        appActivity: 'com.leapxpertapp.MainActivity',
        noReset: true,
        fullReset: false,
        automationName: 'UIAutomator2',
        newCommandTimeout: 30000,
      },
    },
    REST: {
      endpoint: 'https://api.adaptavist.io/tm4j/v2',
      onRequest: (request) => {},
    },
  },
  include: {
    I: './steps_file.js',
    LoginMobilePage: './pages/login_Mobile.js',
    LoginMobileStep: './steps/login_Mobile.js',
  },
  mocha: {
    reporterOptions: {
      mochaFile: 'output/result.xml',
      reportDir: 'output/result.html',
    },
  },
  bootstrap: 'bootstrap.js',
  teardown: 'bootstrap.js',
  hooks: [],
  gherkin: {
    features: 'features/**/*.feature',
    steps: './step_definitions/**/*.js',
  },
  plugins: {
    screenshotOnFail: {
      enabled: true,
    },
    retryFailedStep: {
      enabled: true,
    },
  },
  tests: './*_test.js',
  name: 'lxp-web-automation',
};