const { I } = inject();

module.exports = {
	login:
	{
		textWelcome: "Welcome",
		skipBtn: "//android.view.ViewGroup[@content-desc=\"tutorial_skip\"]/android.widget.TextView",
		signIn: '//android.view.ViewGroup[@content-desc="login_signIn"]/android.widget.TextView',
		textDashboard: "Chats",
		textLater: "LATER",
		password: '//android.widget.EditText[@content-desc="login_password"]',
		later: '//android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]',
	},

	xpathCode(index) {
		return '//android.widget.EditText[@content-desc=\"activation_' + index + '\"]';
	},

	xpathOtp(index) {
		return '//android.widget.EditText[@content-desc="otp_' + index + '"]';
	},
};

